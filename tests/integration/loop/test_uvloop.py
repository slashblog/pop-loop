import asyncio
import sys

import pytest


@pytest.mark.skipif(
    sys.version_info < (3, 7), reason="uvloop requires at least python 3.7"
)
async def test_backend(hub):
    hub.loop.init.create("uv")
    assert "uvloop.Loop" in repr(hub.pop.Loop)
    assert hub.loop.init.backend() == "unknown"
    assert hub.loop.init.BACKEND == "uv"
    await asyncio.sleep(0, loop=hub.pop.Loop)
