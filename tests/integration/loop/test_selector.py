import asyncio


async def test_backend(hub):
    hub.loop.init.create("selector")
    assert "Selector" in repr(hub.pop.Loop)
    assert hub.loop.init.backend() in ("asyncio", "unknown")
    assert hub.loop.init.BACKEND == "selector"
    await asyncio.sleep(0, loop=hub.pop.Loop)
