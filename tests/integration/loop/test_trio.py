import asyncio
import sys

import pytest


@pytest.mark.skipif(
    sys.version_info < (3, 7), reason="trio requires at least python 3.7"
)
async def test_backend(hub):
    hub.loop.init.create("trio")
    assert "TrioEventLoop" in repr(hub.pop.Loop)
    assert hub.loop.init.backend() in ("trio", "unknown")
    assert hub.loop.init.BACKEND == "trio"
    await asyncio.sleep(0, loop=hub.pop.Loop)
