import asyncio

import pytest

try:
    import PyQt5.QtWidgets as pyqt5

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


def setup_module():
    if not HAS_LIBS:
        pytest.skip("These tests only run with QT pre-installed")


async def test_backend(hub):
    hub.loop.qt.APP = pyqt5.QApplication([])
    hub.loop.init.create("qt")
    assert "Q" in repr(hub.pop.Loop)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "qt"
    await asyncio.sleep(0, loop=hub.pop.Loop)
