import asyncio
import os

import pytest


def setup_module():
    if not os.name == "nt":
        pytest.skip("These tests only run on windows")


async def test_backend(hub):
    hub.loop.init.create("proactor")
    assert isinstance(hub.pop.Loop, asyncio.ProactorEventLoop)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "proactor"
    await asyncio.sleep(0, loop=hub.pop.Loop)
